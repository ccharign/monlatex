#! /usr/bin/python3
# -*- coding: utf-8 -*-




import sys
import os
import subprocess







def rép_build_nom(chemin):
    répertoire=os.path.dirname(os.path.abspath(chemin))
    build=répertoire+"/build/"
    os.makedirs(build,exist_ok=True)
    nom=os.path.splitext(os.path.basename(chemin))[0]
    return répertoire, build, nom


def pour_devoir(chemin, debug=False):
    """ Crée un pdf d'énoncé et un pdf de corrigé."""
    
    répertoire, build, nom = rép_build_nom(chemin)

    entree=open(chemin, "r")
    chemin_sol=build+nom+"Corrige.tex"
    sortie_sol=open(chemin_sol , 'w')
    chemin_enonce=build+nom + "Enonce.tex"
    sortie_enonce=open( chemin_enonce, 'w')

    if debug:
        print("nom du fichier : ", nom,"\n")
        print("pour corrigé : ", chemin_sol,"\n")
        print("pour énoncé : ", chemin_enonce,"\n")
        
    def écrit_ligne(l):
        sortie_sol.write(l)
        sortie_enonce.write(l)

    ligne=entree.readline().strip()
    while ligne!=r"\begin{document}":
        if ligne!=r"\modeCorrige":écrit_ligne(ligne+"\n")
        ligne=entree.readline().strip()
    écrit_ligne(ligne+"\n") # Le \begin{document}
    sortie_sol.write(r"\modeCorrige"+"\n")
    sortie_enonce.write(r"\modeEnonce"+"\n")

    for ligne in entree:
        if ligne.strip()!=r"\modeCorrige":écrit_ligne(ligne)

    sortie_sol.close()
    sortie_enonce.close()
    entree.close()
    compileFinale([chemin_enonce, chemin_sol], build, répertoire)


    

def pour_colle(chemin, nb=2):
    """ Crée un pdf contenant deux énoncés et un corrigé.
        Le fichier d'entrée ne doit contenir que les exercices : pas l'entête ni le \begin et \end{document}"""
    répertoire, build, nom = rép_build_nom(chemin)
    cheminSortie = build+nom+"Colle.tex"
    
    entrée=open(chemin,'r')
    sortie=open(cheminSortie, 'w')
    
    contenu=entrée.readlines()
    def écrire_une_copie():
        for l in contenu :sortie.write(l)

    sortie.write(r"""
\documentclass{mon-style}
\begin{document}
\modeEnonce
     """ )

    
    écrire_une_copie()
    for i in range(nb):
        sortie.write(r"""
\setcounter{Exercice}{0}
\newpage
        """)
        écrire_une_copie()
        sortie.write("\n")

    sortie.write(r"\modeCorrige"+"\n")


    for i in range(nb):
        sortie.write(r"""
\setcounter{Exercice}{0}
\newpage
""")
        écrire_une_copie()

    sortie.write(r"\end{document}")
    sortie.close()

    compileFinale([cheminSortie], build, répertoire)

def compileFinale(fichiers, build, rép):
    """ Compile les fichiers .tex dans la liste passée en argument.
        build est le dossier où sont envoyés les fichiers créés.
        rép est le répertoire où les pdf seront déplacés."""
    os.chdir(build)
    for f in fichiers:
        subprocess.call(["pdflatex", f])
        pdf=os.path.splitext(os.path.split(f)[1])[0]+".pdf" #nom du fichier pdf créé
        subprocess.call(["mv",os.path.join(build, pdf),os.path.join(rép, pdf)]) # Les pdf sont déplacés de build vers le répertoire principal
    os.chdir(répertoire)





if len(sys.argv)==1:
    print("pas d'argument reçu")
    print("""
Utilisation:

    monLatex chemin
        compilation type DS, avec un énoncé et un corrigé dans deux pdf différents
        
    monLatex chemin n
         compilation type colle, avec (n+1) énoncés et n corrigés, tout dans le même pdf
         """)
elif len(sys.argv)==2:
        chemin=sys.argv[1]
        pour_devoir(chemin)
elif len(sys.argv)==3: #Utilisation pour une colle
    chemin=sys.argv[1]
    nombre=int(sys.argv[2])
    pour_colle(chemin, nombre)
else:
    print("trop d'arguments")
