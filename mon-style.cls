\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{mon-style}[20/04/2019, v1.0]%
\AtEndOfClass{\RequirePackage{microtype}}%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}%
\ProcessOptions*%
\LoadClass[11pt]{article}%
%========================================
% packages
%========================================
\RequirePackage{ifluatex, ifxetex}
\ifluatex
  \RequirePackage{shellesc}
\else\ifxetex
  \relax
\else % vieux TeX
  \RequirePackage[utf8]{inputenc} 		% utf8
  \RequirePackage[T1]{fontenc} 			% encodage 8-bit avec 256 glyphes
\fi\fi
\RequirePackage{lmodern}
\RequirePackage[french]{babel} 		% français
\RequirePackage{xspace}
%\RequirePackage{amsmath}

\RequirePackage{geometry} 				% géométrie de la page
\RequirePackage{csquotes} 				% gestion des chevrons, etc
\RequirePackage{graphicx} 				% graphiques et couleurs
\RequirePackage{fancyhdr}  				% entêtes et bas de pages
\RequirePackage{multicol,lscape}  	
\RequirePackage[pdfstartview = FitH, colorlinks, linkcolor=darkgray]{hyperref}% lien hypertextes
\RequirePackage{siunitx} 				% système d'unités SI
%\RequirePackage{MnSymbol}
\RequirePackage{xcolor}					% gestion de couleurs
\RequirePackage{footnote}% à charger après xcolor. Pour notes en bage de page sortant des minipages.

\RequirePackage{subfig}
\RequirePackage{float}					% pour les flottants
\RequirePackage{enumitem}
\RequirePackage{tikz}					% éléments graphiques tikz
\RequirePackage{tcolorbox}
\usetikzlibrary{arrows,automata}		% utile pour les automates, etc
\tikzset{
 initial text=$ $,
}
%\RequirePackage[scale=1]{ccicons}		% creative commons
%\RequirePackage{titlesec}				% redéfinir les sections, etc 
\RequirePackage{listings}				% affichage des codes
\RequirePackage{amsmath,amsthm,amssymb,amsfonts,esint,dsfont,
eqnarray}
\RequirePackage{answers}   % indications et solutions
 \Newassociation{indication}{Indication}{indic}
 \Newassociation{solution}{Solution}{sol}
 \theoremstyle{definition}
 \newtheorem{Exercice}{Exercice}
  \newtheorem{exemple}{Exemple}
  
\RequirePackage{lastpage}
\RequirePackage{ifthen}

\PassOptionsToPackage{hyphens}{url}%pour autoriser la césure dans une url... cf stackexchange...
\RequirePackage{hyperref}
%\hypersetup{
%    colorlinks,
%    linkcolor={red!50!black},
%    citecolor={blue!50!black},
%    urlcolor={blue!80!black},
%    pdfencoding=auto
%}



\input{symboles-unicode.tex}		% taper directement des symboles utf8
%========================================
% géométrie de la page
%========================================
\geometry{%
	hoffset=0mm, voffset=0mm,%
	paperwidth=210mm, left=15mm, right=15mm,% width = paperwidth - left - right
	paperheight=297mm, top=10mm, bottom=10mm,% height = paperheight - top - bottom
	marginparsep=0mm, marginparwidth=0mm,% textwidth = width - marginparsep - marginparwidth
	headheight=27.8pt, headsep=5mm, footskip=15mm, includehead, includefoot,% textheight = height - headheight - headsep - footskip si les options includehead et includefoot sont présentes
}%
%========================================
% environnements pour les codes
%========================================
% begin{python}[opt] ... \end{python}
% begin{sql}[opt] ... \end{sql}
%----------------------------------------
% quelques couleurs et réglages
\definecolor{commentsColor}{rgb}{0, .4, 0}
\definecolor{keywordsColor}{rgb}{0.5, 0, 0}
\definecolor{stringColor}{rgb}{0.5, 0, 0}
\DeclareCaptionFont{white}{\color{black}}
\DeclareCaptionFont{red}{\color{red!50!black}}
%
\renewcommand{\lstlistingname}{Code}
\lstset{
  backgroundcolor = \color{white!97!black},
  basicstyle = \ttfamily,	
  breakatwhitespace = false,         
  breaklines = true,                 
  captionpos = t,                    
  commentstyle = \color{commentsColor},
  escapeinside = {\%*}{*)},          
  extendedchars = true,              
  frame = tb,	                   	 
  framerule = .5pt,				
  keepspaces = true,                 
  keywordstyle = \color{keywordsColor}\bfseries,
  numbers = left,                    
  numbersep = 5pt,                   
  numberstyle = \tiny\color{black}, 
  rulecolor = \color{black},         
  showspaces = false,                
  showstringspaces = false,          
  showtabs = false,                  
  stepnumber = 1,                    
  stringstyle = \color{stringColor}, 
  tabsize = 2,	                   
  columns = fixed,                    
  postbreak = \mbox{\textcolor{commentsColor}{$\hookrightarrow$}\space},
  tabsize  =  2,
  mathescape,
  %upquote = true
}
%----------------------------------------
% caractères spéciaux
\lstset{literate =
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {Ã}{{\~A}}1 {ã}{{\~a}}1 {Õ}{{\~O}}1 {õ}{{\~o}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\euro}}1 {£}{{\pounds}}1 {«}{{\guillemotleft}}1
  {»}{{\guillemotright}}1 {ñ}{{\~n}}1 {Ñ}{{\~N}}1 {¿}{{?`}}1
}
%----------------------------------------


\frenchbsetup{ItemLabeli=$\bullet$}
\frenchbsetup{ItemLabelii=$\diamond$}
\frenchbsetup{ItemLabeliii=$\circ$}



% -------- réglages -------------
\setlength\delimitershortfall{-1 pt}%pour que les délimiteurs dépassent leur contenu
\let\epsilon=\varepsilon
\let\phi=\varphi
\let\le=\leqslant
\let\leq=\leqslant
\let\ge=\geqslant
\let\geq=\geqslant
\def\int{\intop\nolimits}





%% frise, aussi par François
\usepackage{expl3}
\makeatletter
\ExplSyntaxOn

\cs_new:Nn \frise:n % l'argument est la longueur nominale de la frise 
                    % en fait, la frise sera plus longue de 1 pt (environ)
     {\dim_set:Nn \l_tmpa_dim {#1} 
      %
      % nombre de motifs de la frise :
      \int_set:Nn \l_tmpa_int {\dim_ratio:nn {\l_tmpa_dim + 3.1415pt} {6.2831pt} }
      %
      \dim_set:Nn \l_tmpb_dim {6.2831pt * \l_tmpa_int}
      %
      \frise_bis:nn \l_tmpa_int {\fp_to_decimal:n {\l_tmpa_dim / \l_tmpb_dim}}
     }

% le premier argument est le nombre de motifs, le deuxième est le coefficient de dilatation
% ce coefficient de dilation est très proche de 1 pour avoir une frise
% qui aura exactement la longueur nominale demandée
\cs_new:Nn \frise_bis:nn
     {\begin{tikzpicture}[scale = 0.0352778, yscale = #2]
      \foreach \k in {1,...,#1} 
        {\begin{scope}[shift={(0,\k * (-6.2831))}]
           \fill
           (0,0) .. controls (0.5,0.5)     and (1,1)         .. (1,1.5708)
                 .. controls (1,2.1415)    and (0.5,2.6415)  .. (0,3.1415)
                 .. controls (-0.5,3.6415) and (-1,4.1415)   .. (-1,4.7123)
                 .. controls (-1,5.2815)   and (-0.5,5.7830) .. (0,6.2830)
                 -- (1.4,7.2830) % ici, on dépasse le 6.2830 de 1 pt
                 .. controls (0.9,6.7830)  and (0.4,6.2815)  .. (0.4,5.7123)
                 .. controls (0.4,5.1415)  and (0.9,4.6415)  .. (1.4,4.1415)
                 .. controls (1.9,3.6415)  and (2.4,3.1415)  .. (2.4,2.5708)
                 .. controls (2.4,2)       and (1.9,1.5)     .. (1.4,1); 
         \end{scope}}
      \end{tikzpicture}}


\newsavebox{\frise@box}

\newenvironment {frise}
   {\savenotes
    \mode_if_horizontal:T {\par}
    \parindent \c_zero_skip
    %
    \leavevmode % on a perdu beaucoup de temps avant de mettre ce \leavevmode
    %
    \begin{lrbox}{\frise@box}
    \dim_set:Nn \l_tmpa_dim {\linewidth-8pt}
    \begin{minipage}[t]{\l_tmpa_dim}
    \begin{list}{}{\leftmargin  = \c_zero_skip
                   \rightmargin = \c_zero_skip
                   \itemindent  = \c_zero_skip
                   \topskip     = \c_zero_skip}
    \item}
   {\end{list}
    \dim_gset:Nn \g_tmpa_dim \prevdepth
    \end{minipage}
    \end{lrbox}
    %
    \dim_compare:nNnT \g_tmpa_dim < {2.5 pt}
          {\dim_set:Nn \l_tmpa_dim {2.5 pt - \g_tmpa_dim}
           \sbox{\frise@box}{\vtop{\usebox{\frise@box}
                                   \vskip\l_tmpa_dim}}}
    %
    % hauteur totale de la boîte (et donc longueur nominale de la frise) :
    \dim_set:Nn \l_tmpa_dim 
                {\exp_after:wN \box_dp:N \frise@box + \exp_after:wN \box_ht:N \frise@box}
    %
    \dim_compare:nNnT \l_tmpa_dim > {6.3 pt}
          {\raisebox{-\exp_after:wN \box_dp:N \frise@box}
                    [\exp_after:wN \box_ht:N \frise@box] % on tronque la hauteur de 1 pt
                    {\makebox[8pt][l]{\kern1pt \frise:n \l_tmpa_dim}}}
    \expandafter\box\frise@box
    \spewnotes
    }


\newenvironment{petitComm}%
    {\ifhmode\par\fi
     \unless\if@inlabel\addvspace{\smallskipamount}\fi
     \begin{frise}}
    {\end{frise}
     \vspace{\smallskipamount}}

\ExplSyntaxOff 

\makeatother
%% -------------- frises: fin ---------------------


\newcommand{\comm}[1]{\begin{petitComm}	 #1\end{petitComm}}
\newenvironment{interlude}{\medskip \it }{\medskip}
\newcommand{\inter}[1]{\begin{interlude}\quad #1\end{interlude}}


%----------------------------------------------------
% --------- Gestion des différents modes de compilation---------------
%----------------------------------------------------
\newcommand{\pts}[1]{#1 pt(s)}

\newcommand{\cours}[1]{\medskip {\bf Cours :} #1 \vspace{\espace cm}}
\newenvironment{colle}[1][3]%
	{\begin{landscape} \begin{multicols}{#1}  \setlength{\columnseprule}{.5pt}\setlength{\columnseprule}{.5pt}}%
	{\end{multicols}\end{landscape}}
	
\def\modeEnonce{
	\renewcommand{\pts}[1]{}
	\Opensolutionfile{sol} % Les solutions seront chargées dans un fichier sol.tex via le paquet answers
	\renewenvironment{indication}{
	
	\textit{Indication :}} {}
	 } % Les indications par contre sont affichées

\def\modeCorrige{ 
    \renewenvironment{solution}{\ \\ \textit{solution :}\color{darkgray} \small}  { }  % Solutions affichée
    \renewenvironment{indication}{\color{darkgray} \textit{Indication :} \small}  {\ \\} % Indications affichées
    \renewenvironment{colle}[1][1]{ \renewcommand{\columnbreak}{}} { } % Les colles ne sont plus en paysage
    \renewcommand{\cours}[1]{ } % On supprime la question de cours des colles
    \renewcommand{\vfill}{ }
    \def\espace{0}
}




%----------------------------------------------------
% --------- Raccourcis ----------------
%----------------------------------------------------


\DeclareMathOperator{\e}{e}
\def\cad{\text{c'est-à-dire }}
\def\dt{\d t}
\def\dx{\d x}
\newcommand{\rien}{}
\def\nb{\noindent \textbf{N.B. }}
\newcommand{\cfexo}[1]{\textbf{cf exercice: } \ref{#1}}


\newcommand{\systeme}[1]{\begin{cases}#1\end{cases}}
\newcommand{\cas}[1]{\begin{array}{|ll}#1\end{array}}
\newcommand{\calc}[2][rlc]{\begin{equationarray*}{#1}
\displaystyle #2
\end{equationarray*}}
\def\ds{\displaystyle}
\def\som{\displaystyle \sum}
\def\sumi{\sum_{i=0}^n}
\def\somi{\displaystyle \sum_{i=0}^n}
\def\sumj{\sum_{j=0}^n}
\def\sumk{\sum_{k=1}^n}
\def\maj{\mathcal}
\def\rg{\text{rg}}
\def\id{\text{Id}}
\def\de{\text{deg}}
\def\kx{\K[X]}
\newcommand{\tr}[1]{{\vphantom{#1}}^{\mathit t}{#1}}
\def\ker{\text{Ker}}
\def\mat{\text{Mat}}
\def\can{\text{can}}


\newcommand{\Mat}[1]{
\begin{pmatrix}
#1
\end{pmatrix} 
}

\makeatletter %pour matrice augmentée
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother
%Exemple:
%$\begin{pmatrix}[cc|c]
%  1 & 2 & 3\\
%  4 & 5 & 9
%\end{pmatrix}$
\newcommand{\Mataug}[2]{
\begin{pmatrix}[#1]
#2
\end{pmatrix}
}

\newcommand{\Det}[1]{
\begin{vmatrix}
#1
\end{vmatrix} 
}

\newcommand{\suite}[2][n\in \N]{\left( #2 \right )_{#1} }
\newcommand{\fl}[1]{\overrightarrow{#1}}
\newcommand{\infini}{\infty}
\newcommand{\vide}{\varnothing}
\newcommand{\oni}[1]{o_{n\to \infini}\left (#1\right )}
\newcommand{\Oni}[1]{O_{n\to \infini}\left (#1\right )}
\newcommand{\The}[2][]{\Theta_{#1}\left (#2\right )}

\def\eqni{\underset{{\small n\rightarrow\infty}}{\scalebox{1.5}[1]{$\sim$}}}
\newcommand{\eq}[1][x\to\infty]{  \underset{{\small #1}}{\ \scalebox{1.5}[1]{$\sim$} \ }  }
\newcommand{\tend}[1][x\to\infty]{  \underset{{\small #1}}{\longrightarrow}  }
\newcommand{\vers}[1]{\underset{{\small #1}}{\rightarrow} }
\newcommand{\dl}[1][n\to\infty]{\underset{\small #1}{=}   }
\def\limn{\lim_{n\to\infini}}

\newcommand{\tq}{\text{ tq }}
\newcommand{\et}{\text{ et }}
\newcommand{\ou}{\:\text{ ou }\:}
%\newcommand{\si}{\:\text{ si }\:}
\newcommand{\non}{\text{ non }}
\newcommand{\dc}{\text{ donc }}
\def\sinon{\text{ sinon }}
\newcommand {\inv} {^{-1}}
\def\rema{\noindent\textit{Remarque :}}
\def\ex{\noindent\textit{Exemple :}}
\newcommand{\exs}[1]{ \textit{Exemples: }\begin{enumerate}#1\end{enumerate} }

\def\preambule{ {\it Si vous repérez ce qui vous paraît une erreur d'énoncé, indiquez-le sur votre copie et précisez les initiatives que vous avez été amenés à prendre.  Vous pouvez coder toute fonction complémentaire qui vous semble utile. Dans ce cas indiquez précisément le rôle de cette fonction, la signification de ses paramètres et la nature de la valeur renvoyée.}}

\newcommand{\N}{\mathbb N}
\def\K{\mathbb K}
\newcommand{\R}{\mathbb R}
\newcommand{\Z}{\mathbb Z}
\newcommand{\Q}{\mathbb Q}
\newcommand{\C}{\mathbb C}
\renewcommand{\P}{\maj P}
\def\L{\mathcal L}
\def\eps{\varepsilon}
\def\lip{lipschitzienne}

\def\rpe{\R^{+*}}

\newcommand{\multi}[2][2]{\begin{multicols}{#1}#2 \end{multicols}}


\newcommand{\ens}[2]{ \left\{ \left.  #1 \;  \middle |  \;\right.  #2 \right\} }
\newcommand{\ensp}[2]{ \left\{ #1 \; ; \; #2 \right\} }
\def\llbracket{[|} % En attendant mieux...
\def\rrbracket{|]}
\newcommand\ent[1] {\llbracket #1 \rrbracket}
\newcommand\entso[1] {\llbracket #1 \llbracket}
\newcommand{\pe}[1]{\left\lfloor\:  #1\: \right\rfloor } 
\newcommand{\ps}[2]{\left\langle \left . #1 \mid  #2 \right . \right\rangle}
\newcommand{\norme}[1]{\left \|#1 \right \|}
\newcommand {\fonc}[4] {
\begin{array}{rcl}
 {#1} & \rightarrow & {#2} \\
 {#3} & \mapsto     & {#4}
\end{array}}



\newcommand{\prop}[1]{\begin{proposition}#1\end{proposition}}
\newcommand{\exo}[2][]{\begin{Exercice}\textbf{#1}\\ #2\end{Exercice}}

%----------------------------------------------------
% --------------- environnements --------------------
%----------------------------------------------------


\newcommand{\cqfd}{\hfill $\square$\\}
\newenvironment{demo}{ \setcounter{equation}{0}\textit{Démonstration: } \small }{\cqfd}

\theoremstyle{plain}
\newtheorem{theoreme}{Théorème} [section]
\newtheorem{definition}[theoreme]{Définition}
\newtheorem{definitions}[theoreme]{Définitions}
%\theoremstyle{definition}
\newtheorem{proposition}[theoreme]{Proposition}
\newtheorem{lemme}[theoreme]{Lemme}

\newtheorem{corollaire}[theoreme]{Corollaire}


\lstnewenvironment{python}[1][]{\lstset{language = python, #1}}{}
\lstnewenvironment{sql}[1][]{\lstset{language = sql, #1}}{}
\lstnewenvironment{html}[1][]{\lstset{language = html, #1}}{}
\def\|{\lstinline[language=python]|} %|





